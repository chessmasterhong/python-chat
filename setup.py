import os
import multiprocessing  # needed to fix interpreter shutdown exception
from setuptools import setup, find_packages

ROOT = os.path.abspath(os.path.dirname(__file__))
#README = open(os.path.join(ROOT, 'README.rst')).read()

# they are required for this project to work
# will install into global / virtualenv python environment
install_requires = [
    'twisted',
]

# will be downloaded to the project as egg file if not already installed
# under the environment the tests are running (global python or virtualenv)
# so we can run ``python setup.py test``. Note that most nosetests plugins
# such as coverage and --with-progressive are not usable with setuptools's
# test method. So you have to install nose in the virtualenv, which then
# it can read setup.cfg (where you specify the beahvior of nose)
test_requires = [
    'mock',
    'nose',
]

# test_suite uses nose so to run tests we should call  ``python setup.py test``
# this is based Sentry's reocmmendation, see https://github.com/getsentry/sentry/blob/master/setup.py
setup(name='chat',
      version='0.0',
      description='chat',
      author='CCNY ACM Python Workshop',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      tests_require=test_requires,
      test_suite="nose.collector",
)

Python Chat Server
==================

This simple IRC-kind chat server is being developed collaboratively with the CCNY ACM Python Workshop in Spring 2013.

You can see features in the `wiki <https://bitbucket.org/chessmasterhong/python-chat/wiki/specs>`_.

Installation
------------

This package comes with a typical `setup.py`::
    
    git clone https://bitbucket.org/chessmasterhong/python-chat
    cd python-chat
    [sudpo] python setup.py [develop|install]
    python setup.py test

You should do this with `develop` if you are developing. You should use virtualenv.

Have fun!

Authors: Kevin Chan, Ian McBride, Yeukhon Wong

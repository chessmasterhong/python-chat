#!/usr/bin/env python2

from twisted.internet import reactor
from twisted.internet.protocol import Protocol, Factory

class ChatProtocol(Protocol):
    def dataReceived(self, message):
        self.transport.write(message) # Echo any message received

if __name__ == '__main__':
    factory = Factory()
    factory.protocol = ChatProtocol
    reactor.listenTCP(8803, factory)

    print "SERVER: Chat server running..."
    reactor.run()

